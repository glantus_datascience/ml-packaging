# importing modules
import pysftp
import os
import sys
import tempfile
import shutil
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mlflow
import mlflow.pyfunc
import pysftp
import json


# function for pysftp
def ml_pysftp(RemoteFilePath):
    with open('/home/jovyan/authentication.txt',"r") as file:
        data = file.readlines()
        password = data[0]
    
    sftp = pysftp.Connection(host="sftp.demo.glantus.com", username= "ml", password=password)
    localFilePath = tempfile.mkdtemp()
    filename = os.path.basename(RemoteFilePath)
    dest_temp_path = os.path.join(localFilePath,filename)
    print(localFilePath)
    sftp.get(RemoteFilePath, dest_temp_path)
    print("Connection succesfully stablished ... ")
    os.chdir(localFilePath)
    sftp.close()    
    
            
# function for mlflow initialisation
def ml_mlfow_init(SettingTracking_Server,ExperimentName):
    mlflow.set_tracking_uri(SettingTracking_Server)
    mlflow.set_experiment(ExperimentName)


# Reading function for different types of files (csv, xlsx, pkl)
def ml_read_data(file):
    if file.lower().endswith('.csv'):
        data = pd.read_csv(file)
    elif file.lower().endswith('.xlsx'):
        data = pd.read_excel(file)
    elif file.lower().endswith('.pkl'):
        data = pd.read_pickle(file)
    else:
        return 'This file type is not handled in the read funtion, give the relevent File extension'
    return data


## Function for deviding the dataset in the train and test split with percentage of data used in test
def ml_train_test_split(data, test_percent):
    temp = round(len(data)*((test_percent)/100))
    temp = len(data)-temp
    train = data.iloc[:temp]
    test = data.iloc[temp:]
    return (train, test)


## Function for saving the model summary
def ml_saving_model_summary(model, path):
    # Generate model summary as text
    with open(os.path.join(path, 'summary.txt'), 'wt') as file:
        file.write(model.summary().as_text())


## setting up the variable
conda_env = {
    'name': 'mlflow-env',
    'channels': ['defaults', 'conda-forge'],
    'dependencies': [
        'python=3.8.1',
        'statsmodels=0.11.0',
        'gunicorn=20.0.4',
        'mlflow=1.6.0'
    ]
}