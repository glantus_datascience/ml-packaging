import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mlgeneric", # Replace with your own username
    version="0.0.1",
    author="Glantus Team",
    description="Generic machine learning packages",
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['pysftp','pandas','numpy','matplotlib','mlflow','sklearn','missingno','seaborn'],
    
)